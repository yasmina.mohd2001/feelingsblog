<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title=$this->faker->unique()->sentence();
        return [
            'title' => Str::slug($title),
            'excerpt' =>$this->faker->paragraph(1),
            'post_image' => $this->faker->image(),
            'post_writer' => $this->faker->word(),
            'category_id' => rand(1,10),
            'post' => $this->faker->paragraph(3)
        ];
    }
}
