<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublishingHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publishing__houses', function (Blueprint $table) {
            $table->id();
            $table->string('bublishing_name')->unique();
            $table->string('place')->nullable();
            $table->string('DateEstablishment')->nullable();
            $table->text('bublishing_image')->nullable();
            $table->text('info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publishing__houses');
    }
}
